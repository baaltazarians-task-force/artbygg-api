<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    public function bgImg (Request $req)
    {
       $path = storage_path('app/bg-images/logon.jpg');

       header('Content-Type: image/jpeg');
       echo file_get_contents($path);
    }
}
