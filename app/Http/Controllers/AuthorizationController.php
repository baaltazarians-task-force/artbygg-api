<?php

namespace App\Http\Controllers;

use App\Mail\PasswordReset as PasswordResetMail;
use App\Models\Notification;
use App\Models\TokenBlackListEntry;
use Illuminate\Http\Request;
use ReallySimpleJWT\Token;

use App\Models\User;
use App\Models\PasswordReset;
use App\Traits\Helpers\PasswordHelper;
use App\Traits\Helpers\RequestHelper;
use App\Traits\Helpers\UrlHelper;
use App\Traits\NotificationTrait;
use App\Traits\SendEmailTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class AuthorizationController extends Controller
{
    use NotificationTrait;
    use SendEmailTrait;
    use PasswordHelper;
    use RequestHelper;
    use UrlHelper;


    const validateFields = [
        "login" => [
            "email" => "required|email",
            "password" => "required|string"
        ],
        "changePassword" => [
            "password" => "required|string"
        ],
        "passwordResetHandler" => [
            "password" => "required|string"
        ]
    ];

    public function login (Request $req)
    {
        $email = $req->input('email');
        $password = $req->input('password');

        $user = User::where('email', $email)->with('auth')->first();
        if (is_null($user) || !$user->auth || !$user->status->active || !Hash::check($password, $user->auth->password)) {
            return $this->responseError('user not found');
        }

        $cacheName = 'cache_'.$user->id;
        $token = $this->createToken($user->id, ['cid' => $cacheName]);

        // Cache::put($cacheName, $user->getPermissionsAttribute(), env('JWT_EXPIRATION'));

        // check if password must be changed
        if ($user->auth->password_reset_on_logon) {
            return response()->json(['status' => true, 'token' => $token, 'password_change' => true]);
        }
        return response()->json(['status' => true, 'token' => $token]);
    }

    public function logout (Request $req)
    {
        $blacklistEntry = new TokenBlackListEntry();
        $blacklistEntry->token = $req['jwt-token'];

        if (!$blacklistEntry->save()) {
            return $this->responseErrorServer('Can not save to database');
        }

        return $this->responseSuccess('succesfully logged out');
    }

    public function userInfo (Request $req)
    {
        $user = $this->retriveUID($req, true);
        if (is_null($user)) return $this->responseError('user not found');

        return $this->responseSuccess($user);
    }

    public function userPermissions (Request $req)
    {
        $userId = $req['jwt-token-payload']['uid'];
        $user = User::where('id', $userId)->first();
        if (is_null($user)) return $this->responseError('user not found');

        return $this->responseSuccess(['permissions' => $user->permissions, 'groups' => $user->groups]);
    }

    public function checkToken (Request $req)
    {
        $token = $req['jwt-token'];
        $payload = $req['jwt-token-payload'];
        $secred = env('JWT_SECRET');
        $now = time() - 5;

        if (Token::validate($token, $secred) && $payload['exp'] > $now) {
            return $this->responseSuccess('Token is valid');
        }
        return $this->responseSuccess('Token is not valid', 201);
    }

    public function extendToken (Request $req)
    {
        $payload = $req['jwt-token-payload'];
        $blacklistEntry = new TokenBlackListEntry();
        $blacklistEntry->token = $req['jwt-token'];

        if (!$blacklistEntry->save()) {
            return $this->responseErrorServer('Can not save to database');
        }

        return response()->json(['status' => true, 'token' => $this->createToken($payload['id'])]);
    }

    public function changePassword (Request $req)
    {
        $password = $req->input('password');

        $user = $this->retriveUID($req, true);
        if (is_null($user)) return $this->responseError('User not found');
        
        if (Hash::check($password, $user->auth->password)) {
            return $this->responseError('New password can not be the same as current one');
        }

        $user->auth->password = Hash::make($password);
        $user->auth->password_reset_on_logon = 0;

        if (!$user->auth->save()) {
            return $this->responseError('Can not save to database');
        }

        // notify user about password change
        $this->userChangesPasswordNotification($user, false);
        $this->sendUserPasswordChangeEmail($user);

        // notify admins about password change
        $this->userChangesPasswordNotification($user, true);

        return $this->responseSuccess();
    }

    public function passwordReset (Request $req)
    {
        $email = $req->input('email');
        $user = User::where('email', $email)->with('auth')->first();
        if (is_null($user)) {
            sleep(1);
            return $this->responseSuccess('Password reset link send');
        }

        // delete all password reset tokens
        PasswordReset::where('auth_id', $user->auth->id)->delete();

        $passReset = new PasswordReset();
        $passReset->auth_id = $user->auth->id;
        $passReset->reset_token = $this->generatePassword(20);
        $passReset->valid_until = time() + intval(env('PASSWORD_RESET_TOKEN_EXPIRATION'));

        if (!$passReset->save()) {
            return $this->responseError('Can not create password reset token');
        }

        // send message with password reset link
        $url = $this->createResetPasswordUrl($passReset);
        $this->sendUserPasswordResetLinkEmail($user, $url);

        return $this->responseSuccess('Password reset link send');
    }

    public function checkPasswordReset (Request $req, $token = null)
    {
        $passReset = PasswordReset::where('reset_token', base64_decode($token))->first();
        if (is_null($passReset) || $passReset->valid_until < time() - 60) {
            return $this->responseError("Token is not valid");
        }
        return $this->responseSuccess("Token is valid");
    }

    public function passwordResetHandler (Request $req, $token = null)
    {
        $passReset = PasswordReset::where('reset_token', base64_decode($token))->first();
        $password = $req->input('password');

        if (is_null($passReset) || $passReset->valid_until < time() - 60) {
            return $this->responseError("Token is not valid");
        }

        if (is_null($passReset->auth)) {
            return $this->responseError("Token is not valid");
        }

        $passReset->auth->password = Hash::make($password);

        if (!$passReset->auth->save()) {
            return $this->responseError("Can not save to database");
        }

        // notify user about password change
        $this->userChangesPasswordNotification($passReset->auth->user, false);
        $this->sendUserPasswordChangeEmail($passReset->auth->user);

        // notify admins about password change
        $this->userChangesPasswordNotification($passReset->auth->user, true);

        $passReset->delete();
        return $this->responseSuccess("Succesfully changed password");
    }

    private function createToken ($userId, $payload = [])
    {
        $secret = env('JWT_SECRET');
        $issuer = env('JWT_ISSUER');
        $expiration = time() + intval(env('JWT_EXPIRATION'));

        $payload['iat'] = time();
        $payload['uid'] = $userId;
        $payload['exp'] = $expiration;
        $payload['iss'] = $issuer;

        return Token::customPayload($payload, $secret);
    }
}
 