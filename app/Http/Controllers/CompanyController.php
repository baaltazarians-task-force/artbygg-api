<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    const resultMaxLimit = 100;
    const filterConditions = [];

    const requiredPermissions = [
        'getCompanies' => ['full' => ['companies.read'], 'partial' => ['!users.create', '!users.update']]
    ];

    public function getCompanies (Request $req)
    {
        return $this->responseSuccess(Company::all());
    }
}
