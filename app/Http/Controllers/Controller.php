<?php

namespace App\Http\Controllers;

use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use ResponseTrait;


    const validateFields = [];
    const requiredPermissions = [];
    protected bool $isPartialRequest;

    public function __construct(Request $req)
    {
        $this->isPartialRequest = isset($req['partial-request']) && $req['partial-request'];
    }

}
