<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\UserGroup;
use App\Traits\PermissionTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    use PermissionTrait;

    const resultMaxLimit = 100;
    
    const requiredPermissions = [
        'getGroups'   => ['full' => ['groups.read'],   'partial' => ['!users.create', '!users.update']],
        'getGroup'    => ['full' => ['groups.read'],   'partial' => null],
        'createGroup' => ['full' => ['groups.create'], 'partial' => null],
        'updateGroup' => ['full' => ['groups.update'], 'partial' => null],
        'deleteGroup' => ['full' => ['groups.delete'], 'partial' => null]
    ];

    const validateFields = [
        'createGroup' => [
            'slug'                     => 'required|string|unique:user_groups,slug',
            'description'              => 'string',
            'add_permissions'          => 'array',
            'add_permissions.*.id'     => 'exists:permissions,id',
            'add_permissions.*.create' => 'required|boolean',
            'add_permissions.*.read'   => 'required|boolean',
            'add_permissions.*.update' => 'required|boolean',
            'add_permissions.*.delete' => 'required|boolean',
            'add_permissions.*.other'  => 'required|boolean',
        ],
        'updateGroup' => [
            'slug'                     => 'unique:user_groups,slug',
            'description'              => 'string',
            'remove_permissions'       => 'array',
            'remove_permissions.*'     => 'exists:permissions,id',
            'add_permissions'          => 'array',
            'add_permissions.*.id'     => 'exists:permissions,id',
            'add_permissions.*.create' => 'required|boolean',
            'add_permissions.*.read'   => 'required|boolean',
            'add_permissions.*.update' => 'required|boolean',
            'add_permissions.*.delete' => 'required|boolean',
            'add_permissions.*.other'  => 'required|boolean',
        ]
    ];

    // get all (or searched) groups
    public function getGroups (Request $req)
    {
        // Get request modifiers
        $page         = intval($req->input('page')) ?? 1;
        $limit        = intval($req->input('limit'));
        $search       = $req->input('search');
        $deleted      = filter_var($req->input('deleted'), FILTER_VALIDATE_BOOLEAN);
        $relations    = $req->input('relation');
        $attributes   = $req->input('attribute');

        // convert or set request result limit to right one
        $limit = !$limit || $limit > self::resultMaxLimit ? self::resultMaxLimit : $limit;

        // convert relation and attribute modifier to array
        $relations  = !is_null($relations) && !is_array($relations) ? [0 => $relations] : $relations;
        $attributes = !is_null($attributes) && !is_array($attributes) ? [0 => $attributes] : $attributes;

        // start building query
        $groupQuery = UserGroup::query();

        // search groups also with deleted_at flag set
        if ($deleted) {
            $groupQuery->withTrashed();
        }

        // search groups by given value
        if ($search) {
            $groupQuery->where('slug', 'like', "%$search%");
        }

        // attach requested relations only when they exist and request maker has special permissions
        if (is_array($relations) && !$this->isPartialRequest && $this->testPermissions($req['user-permissions'], ['groups.other'])) {
            foreach ($relations as $rel) {
                if (method_exists(UserGroup::class, $rel)) {
                    $groupQuery->with($rel);
                }
            }
        }

        // get maximum pages available
        $maxPages = ceil($groupQuery->count() / $limit);
        
        // set pagination and limits
        $groupQuery->take($limit);
        $groupQuery->skip($page - 1); // pages counts from 0

        // if partial request limit columns to selected ones
        if ($this->isPartialRequest) {
            $groupQuery->select(UserGroup::partialSelectShow);
        }

        // get searched groups
        $groups = $groupQuery->get();
        if (is_null($groups) || !sizeof($groups)) return $this->responseError('Groups not found');

        // hide unwanted fields if partial request
        if ($this->isPartialRequest) {
            $groups->each(function ($group) {
                $group->makeHidden(UserGroup::partialSelectHide);
            });
        }

        // attach request attributes only when they exist and request maker has special permissions
        if (is_array($attributes) && !$this->isPartialRequest && $this->testPermissions($req['user-permissions'], ['groups.other'])) {
            foreach ($attributes as $attr) {
                if (method_exists(UserGroup::class, 'get'.$attr.'attribute')) {
                    $groups->each(function ($group) use ($attr) {
                        $group->append($attr);
                    });
                }
            }
        }

        return $this->responseSuccess(['groups' => $groups, 'max_pages' => $maxPages]);
    }

    // get specific group
    public function getGroup (Request $req, $id)
    {
        // Get request mogivendifiers
        $deleted = filter_var($req->input('deleted'), FILTER_VALIDATE_BOOLEAN);
        $relations = $req->input('relation');
        $attributes = $req->input('attribute');

        // convert relation and attribute modifier to array
        $relations = !is_null($relations) && !is_array($relations) ? [0 => $relations] : $relations;
        $attributes = !is_null($attributes) && !is_array($attributes) ? [0 => $attributes] : $attributes;

        // start building query
        $groupQuery = UserGroup::query();
        $groupQuery->where('id', $id);

        // search groups also with deleted_at flag set
        if ($deleted) {
            $groupQuery->withTrashed();
        }

        // attach requested relations only when they exist and request maker has special permissions
        if (is_array($relations)  && !$this->isPartialRequest && $this->testPermissions($req['user-permissions'], ['groups.other'])) {
            foreach ($relations as $rel) {
                if (method_exists(UserGroup::class, $rel)) {
                    $groupQuery->with($rel);
                }
            }
        }

        // if partial request limit columns to selected ones
        if ($this->isPartialRequest) {
            $groupQuery->select(UserGroup::partialSelectShow);
        }

        // search for given group
        $group = $groupQuery->first();
        if (is_null($group)) return $this->responseError('Group not found');

        // hide unwanted fields if partial request
        if ($this->isPartialRequest) {
            $group->makeHidden(UserGroup::partialSelectHide);
        }

        // attach request attributes only when they exist and request maker has special permissions
        if (is_array($attributes)  && !$this->isPartialRequest && $this->testPermissions($req['user-permissions'], ['groups.other'])) {
            foreach ($attributes as $attr) {
                if (method_exists(UserGroup::class, 'get'.$attr.'attribute')) {
                    $group->append($attr);
                }
            }
        }

        // return requested user with/without relations/attributes
        return $this->responseSuccess($group);
    }

    // create new group
    public function createGroup (Request $req)
    {
        // fields
        $slug              = $req->input('slug');
        $description       = $req->input('description');
        $attachPermissions = $req->input('add_permissions') ?? [];

        /// begin database transaction
        DB::beginTransaction();

        // assign values to UserGroup fields
        $newGroup = new UserGroup();
        
        $newGroup->slug        = $slug;
        $newGroup->description = $description;

        if (!$newGroup->save()) {
            DB::rollBack();
            return $this->responseErrorServer("Can not save to database");
        }

        // attach submitted permissions to new group only if they exist
        foreach ($attachPermissions as $values) {
            // attach permission with all values
            $newGroup->permissions()->attach($values['id'], [
                'create' => $values['create'],
                'read'   => $values['read'],
                'update' => $values['update'],
                'delete' => $values['delete'],
                'other'  => $values['other']
            ]);

            // check if permissions was attached
            $check = DB::table('user_groups_permissions_pivot')
                ->where('user_group_id', $newGroup->id)
                ->where('permission_id', $values['id'])
                ->where('create', $values['create'])
                ->where('read',   $values['read'])
                ->where('update', $values['update'])
                ->where('delete', $values['delete'])
                ->where('other',  $values['other'])
                ->first();

            if (is_null($check)) {
                DB::rollBack();
                return $this->responseErrorServer("Can not attach permission to group");
            }
        }

        // save all changes made in database
        DB::commit();

        return $this->responseSuccess(['group' => UserGroup::find($newGroup->id)]);
    }

    // update specific group informations 
    public function updateGroup (Request $req, $id)
    {
        // standart fields
        $fields['slug']        = $req->has('slug')        ? $req->input('slug')        : false;
        $fields['description'] = $req->has('description') ? $req->input('description') : false;

        // permissions to add or remove from group
        $attachPermissions = $req->input('add_permissions')    ?? [];
        $detachPermissions = $req->input('remove_permissions') ?? [];

        // search for group which will be edited
        $group = UserGroup::find($id);
        if (is_null($group)) {
            return $this->responseError('Group not found');
        }

        // begin database transaction
        DB::beginTransaction();

        // change all submited fields
        foreach ($fields as $field => $value) {
            if ($value === false) continue;
            $group->{$field} = $value;
        }

        // save changes, if fail rollback changes in database and report an error
        if (!$group->save()) {
            DB::rollBack();
            return $this->responseErrorServer('Can not save to database');
        }

        // only remove permissions that exists
        foreach ($detachPermissions as $permission) {
            $group->permissions->contains($permission) ? $group->permissions()->detach($permission) : null;
        }

        // only add permissions that exists
        foreach ($attachPermissions as $permissionId => $values) {
            !$group->permissions->contains($permissionId) ?
                $group->permissions()->attach($values['id'], [
                    'create' => $values['create'],
                    'read'   => $values['read'],
                    'update' => $values['update'],
                    'delete' => $values['delete'],
                    'other'  => $values['other']
                ])
                : null;
        }

        // save changes made in database
        DB::commit();

        return $this->responseSuccess(UserGroup::find($group->id));
    }

    // soft delete (attach deleted_at flag in database) specific users
    public function deleteGroup (Request $req, $id)    
    {
        // Find given group
        $group = UserGroup::find($id);
        if (is_null($group)) {
            return $this->responseError('Group not found');
        }

        // Soft delete from databse
        if (!$group->delete()) {
            return $this->responseErrorServer('Can not delete from database');
        }

        return $this->responseSuccess();
    }

}
