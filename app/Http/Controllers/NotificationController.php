<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\Helpers\RequestHelper;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    use RequestHelper;

    public function getNotifications (Request $req)
    {
        $user = $this->retriveUID($req, true);
        if (is_null($user)) return $this->responseError('user not found');

        return $this->responseSuccess($user->notifications);
    }
}
