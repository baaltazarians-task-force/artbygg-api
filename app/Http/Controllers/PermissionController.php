<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permission;

class PermissionController extends Controller
{
    const requiredPermissions = [
        'getPermissions' => ['full' => ['!groups.create', '!groups.update'], 'partial' => null]
    ];

    public function getPermissions (Request $req)
    {
        return $this->responseSuccess(Permission::all());
    }
}
