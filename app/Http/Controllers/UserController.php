<?php

namespace App\Http\Controllers;

use App\Models\Authorization;
use App\Models\Company;
use App\Models\User;
use App\Models\UserGroup;
use App\Models\WorkPosition;
use App\Models\UserStatus;

use App\Traits\NotificationTrait;
use App\Traits\PermissionTrait;
use App\Traits\SendEmailTrait;
use App\Traits\Helpers\PasswordHelper;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use SendEmailTrait;
    use PermissionTrait;
    use NotificationTrait;
    use PasswordHelper;

    const resultMaxLimit = 100;

    const filterConditions = [
        "userGroup" => ["relation" => true, "field" => "groups"],
        "company"   => ["relation" => true, "field" => "company"],
        "status"    => ["relation" => true, "field" => "status"]
    ];

    const requiredPermissions = [
        'getUsers'   => ['full' => ['users.read'],   'partial' => null],
        'getUser'    => ['full' => ['users.read'],   'partial' => null],
        'createUser' => ['full' => ['users.create'], 'partial' => null],
        'updateUser' => ['full' => ['users.update'], 'partial' => null],
        'deleteUser' => ['full' => ['users.delete'], 'partial' => null]
    ];

    const validateFields = [
        'createUser' => [
            'first_name'       => 'required|string',
            'last_name'        => 'required|string',
            'email_address'    => 'required|email|unique:users,email',
            'phone'            => 'max:12',
            'company_id'       => 'exists:companies,id',
            'work_position_id' => 'exists:work_positions,id',
            'activate'         => 'boolean',
            'group_id'         => 'exists:user_groups,id'
        ],
        'updateUser' => [
            'first_name'       => 'string',
            'last_name'        => 'string',
            'email_address'    => 'email|unique:users,email',
            'phone'            => 'max:12',
            'company_id'       => 'exists:companies,id',
            'work_position_id' => 'exists:work_positions,id',
            'changeStatus'     => 'boolean',
            'add_groups'       => 'array',
            'remove_groups'    => 'array',
            'add_groups.*'      => 'exists:user_groups,id',
            'remove_groups.*'   => 'exists:user_groups,id'
        ]
    ];

    // get all (or searched) users
    public function getUsers (Request $req)
    {
        // Get request modifiers
        $page         = intval($req->input('page')) ?? 1;
        $limit        = intval($req->input('limit'));
        $search       = $req->input('search');
        $searchColumn = $req->input('column');
        $deleted      = filter_var($req->input('deleted'), FILTER_VALIDATE_BOOLEAN);
        $relations    = $req->input('relation');
        $attributes   = $req->input('attribute');
        $filters      = $req->input('filter');

        // convert or set request result limit to right one
        $limit = !$limit || ($limit > self::resultMaxLimit ? self::resultMaxLimit : $limit);

        // convert relation and attribute modifier to array
        $relations  = !is_null($relations)  && !is_array($relations)  ? [0 => $relations]  : $relations;
        $attributes = !is_null($attributes) && !is_array($attributes) ? [0 => $attributes] : $attributes;
        $filters    = !is_null($filters)    && !is_array($filters)    ? [0 => $filters]    : $filters;

        // start building query
        $userQuery = User::query();

        // search users also with deleted_at flag set
        if ($deleted) {
            $userQuery->withTrashed();
        }

        // search users by given value
        if ($search) {
            if ($searchColumn) {
                if (method_exists(User::class, $searchColumn)) {
                    // search by relation and its fields
                    $userQuery->whereHas($searchColumn, function ($query) use ($search) {
                        $query
                            ->where('name', 'like', "%$search%")
                            ->orWhere('slug', 'like', "$search");
                    });
                } else {
                    $userQuery->where($searchColumn, 'like', "%$search%");
                }
            } else {
                $userQuery
                    ->where('first_name', 'like', "%$search%")
                    ->orWhere('last_name', 'like', "%$search%")
                    ->orWhere('email', 'like', "%$search%")
                    ->orWhere('phone', 'like', "%$search%")
                    ->orWhereHas('company', function ($query) use ($search) {
                        $query
                            ->where('name', 'like', "$search")
                            ->orWhere('tax_number', 'like', "$search");
                    })
                    ->orWhereHas('workPosition', function ($query) use ($search) {
                        $query->where('pivot', 'like', "$search");
                    });
            }
        }

        // attach requested relations only when they exist and request maker has special permissions
        if (is_array($relations) && !$this->isPartialRequest && $this->testPermissions($req['user-permissions'], ['users.other'])) {
            foreach ($relations as $rel) {
                if (method_exists(User::class, $rel)) {
                    $userQuery->with($rel);
                }
            }
        }

        // filter results by given conditions
        if (is_array($filters)) {
            foreach ($filters as $filter) {
                $filterRaw   = explode('.', $filter);
                $filterKey   = $filterRaw[0];
                $filterValue = $filterRaw[1];
    
                if (isset(self::filterConditions[$filterKey])) {
                    $condition  = self::filterConditions[$filterKey];
                    $isRelation = $condition['relation'];
                    $field      = $condition['field'];
    
                    if ($isRelation) {
                        $userQuery->whereHas($field, function ($query) use ($filterValue) {
                            $query->where('id', $filterValue);
                        });
                    }
                }
            }
        }

        // get maximum pages available
        $maxPages = ceil($userQuery->count() / $limit);

        // set pagination and limits
        $userQuery->take($limit);
        $userQuery->skip($page - 1); // pages counts from 0

        // if partial request limit columns to selected ones
        if ($this->isPartialRequest) {
            $userQuery->select(User::partialSelectShow);
        }

        // get searched users
        $users = $userQuery->get();
        if (is_null($users) || !sizeof($users)) return $this->responseError('Users not found');

        // hide unwanted fields if partial request
        if ($this->isPartialRequest) {
            $users->each(function ($user) {
                $user->makeHidden(User::partialSelectHide);
            });
        }

        // attach request attributes only when they exist and request maker has special permissions
        if (is_array($attributes) && !$this->isPartialRequest && $this->testPermissions($req['user-permissions'], ['users.other'])) {
            foreach ($attributes as $attr) {
                if (method_exists(User::class, 'get'.$attr.'attribute')) {
                    $users->each(function ($user) use ($attr) {
                        $user->append($attr);
                    });
                }
            }
        }

        return $this->responseSuccess(['users' => $users, 'max_pages' => $maxPages]);
    }

    // get only one specific user
    public function getUser (Request $req, int $id)
    {
        // Get request modifiers
        $deleted = filter_var($req->input('deleted'), FILTER_VALIDATE_BOOLEAN);
        $relations = $req->input('relation');
        $attributes = $req->input('attribute');

        // convert relation and attribute modifier to array
        $relations = !is_null($relations) && !is_array($relations) ? [0 => $relations] : $relations;
        $attributes = !is_null($attributes) && !is_array($attributes) ? [0 => $attributes] : $attributes;

        // start building query
        $userQuery = User::query();
        $userQuery->where('id', $id);

        // search user also with deleted_at flag set
        if ($deleted) {
            $userQuery->withTrashed();
        }

        // attach requested relations only when they exist and request maker has special permissions
        if (is_array($relations) && !$this->isPartialRequest && $this->testPermissions($req['user-permissions'], ['users.other'])) {
            foreach ($relations as $rel) {
                if (method_exists(User::class, $rel)) {
                    $userQuery->with($rel);
                }
            }
        }

        // if partial request limit columns to selected ones
        if ($this->isPartialRequest) {
            $userQuery->select(User::partialSelectShow);
        }

        // search for given user
        $user = $userQuery->first();
        if (is_null($user)) return $this->responseError('User not found');

        // hide unwanted fields if partial request
        if ($this->isPartialRequest) {
            $user->makeHidden(User::partialSelectHide);
        }

        // attach request attributes only when they exist and request maker has special permissions
        if (is_array($attributes) && !$this->isPartialRequest && $this->testPermissions($req['user-permissions'], ['users.other'])) {
            foreach ($attributes as $attr) {
                if (method_exists(User::class, 'get'.$attr.'attribute')) {
                    $user->append($attr);
                }
            }
        }

        // return requested user with/without relations/attributes
        return $this->responseSuccess($user);
    }

    // create new user
    public function createUser (Request $req)
    {
        // required fields
        $firstName = $req->input('first_name');
        $lastName  = $req->input('last_name');
        $email     = $req->input('email_address');

        // optional fields
        $phone          = $req->input('phone')            ?? null;
        $companyId      = $req->input('company_id')       ?? null;
        $workPositionId = $req->input('work_position_id') ?? null;
        $activateUser   = $req->input('activate')         ?? false;

        // other optional fields
        $groupId = $req->input('group_id') ?? null;

        // begin database transaction
        DB::beginTransaction();

        // assign values to User fields
        $newUser = new User();
        $newUser->first_name       = $firstName;
        $newUser->last_name        = $lastName;
        $newUser->email            = $email;
        $newUser->phone            = $phone;
        $newUser->company_id       = $companyId;
        $newUser->work_position_id = $workPositionId;

        if (!$newUser->save()) {
            DB::rollBack();
            return $this->responseErrorServer("Can not save to database");
        }

        // generate temporary password for new user (must be changed after first login)
        $password = $this->generatePassword(12);

        // create Auth object for new user
        $newAuth = new Authorization();
        $newAuth->user_id                  = $newUser->id;
        $newAuth->password                 = Hash::make($password);
        $newAuth->password_expires         = false;
        $newAuth->password_reset_on_logon  = true;
        $newAuth->password_expiration_date = null;
        $newAuth->locked                   = false;

        if (!$newAuth->save()) {
            DB::rollBack();
            return $this->responseErrorServer("Can not save to database");
        }

        // add user to submitted group
        if ($groupId) {
            $newUser->groups()->attach($groupId);
            $check = DB::table('user_groups_pivot')
                ->where('user_id', $newUser->id)
                ->where('user_group_id', $groupId)
                ->first();

            if (is_null($check)) {
                DB::rollBack();
                return $this->responseErrorServer("Can not attach group to user");
            }
        }

        // activate user
        $newStatus = new UserStatus();
        $newStatus->user_id = $newUser->id;
        $newStatus->active = $activateUser ? 1 : 0;

        if (!$newStatus->save()) {
            DB::rollBack();
            return $this->responseErrorServer("Can not save to database");
        }

        // save all changes made in database
        DB::commit();

        // Notify admins (users in group administrator) about creating new user
        $this->newUserNotification($newUser);
        $this->sendNewUserEmail($newUser);

        // notify newly created user about account creation
        $this->sendNewUserAccountNotificationEmail($newUser, $password);

        return $this->responseSuccess(['user' => User::find($newUser->id), 'password' => $password]);
    }

    // update specific users informations
    public function updateUser (Request $req, int $id)
    {
        // standard fields
        $fields['first_name'] = $req->has('first_name')    ? $req->input('first_name')    : false;
        $fields['last_name']  = $req->has('last_name')     ? $req->input('last_name')     : false;
        $fields['email']      = $req->has('email_address') ? $req->input('email_address') : false;
        $fields['phone']      = $req->has('phone')         ? $req->input('phone')         : false;

        // relation fields which must be checked before changing
        $companyId      = $req->has('company_id')       ? $req->input('company_id')       : false;
        $workPositionId = $req->has('work_position_id') ? $req->input('work_position_id') : false;

        // status: active / inactive
        $status = $req->input('changeStatus') ?? null;

        // user groups from which user should be added or removed
        $attachGroups = $req->input('add_groups')    ?? [];
        $detachGroups = $req->input('remove_groups') ?? [];

        if (!is_array($attachGroups) || !is_array($detachGroups)) {
            return $this->responseError("List of groups to add or remove should be an array");
        }

        // search for user which will be edited
        $user = User::find($id);
        if (is_null($user)) {
            return $this->responseError('User not found');
        }

        // begin database transaction
        DB::beginTransaction();

        // change all submited fields
        foreach ($fields as $field => $value) {
            if ($value === false) continue;
            $user->{$field} = $value;
        }

        // check if submitted company exists then change it for user
        if ($companyId !== false && $user->company_id != $companyId) {
            if (!is_null($companyId) && is_null(Company::find($companyId))) {
                return $this->responseError('Submitted company does not exist');
            }
            $user->company_id = $companyId;
        }

        // check if submitted work position exists then change it for user
        if ($workPositionId !== false && $user->work_position_id != $workPositionId) {
            if (!is_null($workPositionId) && is_null(WorkPosition::find($workPositionId))) {
                return $this->responseError('Submitted work position does not exist');
            }
            $user->work_position_id = $workPositionId;
        }

        // save changes, if fail rollback changes in database and report an error
        if (!$user->save()) {
            DB::rollBack();
            return $this->responseErrorServer('Can not save to database');
        }

        // only add groups that exists
        foreach ($attachGroups as $group) {
            if (is_null(UserGroup::find($group))) {
                DB::rollBack();
                return $this->responseError("User group not found");
            }
            !$user->groups->contains($group) ? $user->groups()->attach($group) : null;
        }

        // only remove groups that exists
        foreach ($detachGroups as $group) {
            if (is_null(UserGroup::find($group))) {
                DB::rollBack();
                return $this->responseError("User group not found");
            }
            $user->groups->contains($group) ? $user->groups()->detach($group) : null;
        }

        // change user status
        if (!is_null($status)) {
            $user->status->active = $status ? 1 : 0;
            if (!$user->status->save()) {
                DB::rollBack();
                return $this->responseError("Can not change user status");
            }
        }

        // save changes made in database
        DB::commit();

        // notify admins about user change
        $this->userModifyNotification($user);


        return $this->responseSuccess(['user' => User::find($user->id)]);
    }

    // soft delete (attach deleted_at flag in database) specific users
    public function deleteUser (Request $req, int $id)
    {
        // Find given user
        $user = User::find($id);
        if (is_null($user)) {
            return $this->responseError('User not found');
        }

        // Soft delete from databse
        if (!$user->delete()) {
            return $this->responseErrorServer('Can not delete from database');
        }

        // forget cache for (soft) deleted user
        Cache::forget('cache_'.$id);

        // notify admins about user deletion
        $this->userDeletionNotification($user);

        return $this->responseSuccess();
    }
}
