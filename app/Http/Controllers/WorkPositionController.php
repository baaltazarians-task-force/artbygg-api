<?php

namespace App\Http\Controllers;

use App\Models\WorkPosition;
use Illuminate\Http\Request;

class WorkPositionController extends Controller
{
    const resultMaxLimit = 100;
    const filterConditions = [];

    const requiredPermissions = [
        'getWorkPositions' => ['full' => ['!users.create', '!users.update', '!companies.create', '!companies.update'], 'partial' => null]
    ];

    public function getWorkPositions (Request $req)
    {
        return $this->responseSuccess(WorkPosition::all());
    }
}
