<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Traits\ResponseTrait;
use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use ReallySimpleJWT\Token;

class JWTAuth
{
    use ResponseTrait;

    public function handle($request, Closure $next)
    {
        $token_raw = $request->header('authorization');
        if (is_null($token_raw)) return $this->responseError('Token is invalid', 401);
  
        $secret = env('JWT_SECRET');
        $token = substr($token_raw, 7);

        // Check if token is valid
        if (!Token::validate($token, $secret)) return $this->responseError('Token is invalid', 401);

        $payload = Token::getPayload($token, $secret);
        $now = time();

        // Check if token has not expired
        if ($now >= $payload['exp']) return $this->responseError('Token is invalid', 401);

        // Check if token is not blacklisted 
        $blacklist = DB::table('token_blacklist')->where('token', $token)->first();
        if (!is_null($blacklist)) return $this->responseError('Token is invalid', 401);


        $request['jwt-token-payload'] = $payload;
        $request['jwt-token'] = $token;
        $request['user-permissions'] = Cache::get($payload['cid']) ?? null;


        // load user permissions if cache doesn't exists
        if (!$request['user-permissions']) {
            $user = User::find($payload['uid']);
            if (is_null($user)) return $this->responseError('Token is invalid', 401);
            $request['user-permissions'] = $user->getPermissionsAttribute();
        }

        return $next($request);
    }
}
