<?php

namespace App\Http\Middleware;

use App\Traits\PermissionTrait;
use App\Traits\ResponseTrait;
use Closure;

class PermissionMiddleware
{
    use ResponseTrait;
    use PermissionTrait;

    public function handle($request, Closure $next)
    {
        if (!isset($request['user-permissions'])) {
            return $this->responseUnauthorized();
        }

        $handler = $this->getRequestHandler($request);
        if (!$handler) return $next($request);

        $requiredPermissions = $handler['controller']::requiredPermissions ?? null;
        if (is_null($requiredPermissions) || !sizeof($requiredPermissions)) {
            return $next($request);
        }

        $permissions = $requiredPermissions[$handler['method']] ?? null;
        if (is_null($permissions) || !sizeof($permissions)) {
            return $next($request);
        }

        $testPermissions = $this->checkPermissions($request['user-permissions'], $permissions['full'], $permissions['partial']);

        if (!$testPermissions['passes'] && !$testPermissions['partial']) {
            return $this->responseUnauthorized();
        }

        if ($testPermissions['partial']) {
            $request['partial-request'] = true;
        }
        
        return $next($request);
    }

    private function getRequestHandler ($request)
    {
        $raw = $request->route();
        if (!is_array($raw)) return false;

        $raw = $raw[1];
        $uses = $raw['uses'];
        if (!$uses || is_null($uses)) return false;

        $uses = explode('@', $uses);

        return [
            'controller' => $uses[0],
            'method' => $uses[1]
        ];
    }
}
