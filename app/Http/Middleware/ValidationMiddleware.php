<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;
use App\Traits\ResponseTrait;

class ValidationMiddleware
{
    use ResponseTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $handler = $this->getRequestHandler($request);
        if (!$handler) return $next($request);

        $fieldValidationRules = $handler['controller']::validateFields ?? null;
        if (is_null($fieldValidationRules) || !sizeof($fieldValidationRules)) {
          return $next($request);
        }

        $rules = $fieldValidationRules[$handler['method']] ?? null;
        if (is_null($rules) || !sizeof($rules)) {
            return $next($request);
        }

        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            return $this->responseError($validator->messages());
        }

        return $next($request);
    }

    private function getRequestHandler ($request)
    {
        $raw = $request->route();
        if (!is_array($raw)) return false;

        $raw = $raw[1];
        $uses = $raw['uses'];
        if (!$uses || is_null($uses)) return false;

        $uses = explode('@', $uses);

        return [
            'controller' => $uses[0],
            'method' => $uses[1]
        ];
    }
}
