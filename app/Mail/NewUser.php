<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Support\Facades\Mail;

class NewUser
{

    private User $newUser;
    private array $admins;

    function __construct(User $newUser, array $admins = [])
    {
        $this->newUser = $newUser;
        $this->admins = $admins;
    }

    public function send()
    {
        $newUser = $this->newUser;
        $admins = $this->admins;
        Mail::send('email.NewUser', ['user' => $newUser], function ($message) use ($newUser, $admins) {
            $message
              ->to($admins)
              ->subject('New user -> '. $newUser->first_name.' '.$newUser->last_name);
        });

        return true;
    }
}
