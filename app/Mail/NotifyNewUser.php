<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Support\Facades\Mail;

class NotifyNewUser
{

    private User $newUser;
    private string $password;

    function __construct(User $newUser, string $password)
    {
        $this->newUser = $newUser;
        $this->password = $password;
    }

    public function send()
    {
        $newUser = $this->newUser;
        Mail::send('email.NotifyNewUser', [ 'user' => $newUser, 'password' => $this->password ], function ($message) use ($newUser) {
            $message
              ->to($newUser->email)
              ->subject('Hello, You have new account in Artbygg app!');
        });
    }
}
