<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Support\Facades\Mail;

class PasswordReset
{

    private string $url;
    private User $user;

    function __construct(string $generatedURL, User $user)
    {
        $this->url = $generatedURL;
        $this->user = $user;
    }

    public function send()
    {
        $user = $this->user;
        Mail::send('email.PasswordReset', [ 'url' => $this->url, 'user' => $user ], function ($message) use ($user) {
            $message->to(
                $user->email,
                $user->last_name." ".$user->first_name
            )->subject('Password reset');
        });

        return true;
    }
}
