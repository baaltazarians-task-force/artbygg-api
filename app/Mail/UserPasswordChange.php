<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Support\Facades\Mail;

class UserPasswordChange
{

    private User $user;

    function __construct(User $user)
    {
        $this->user = $user;
    }

    public function send()
    {
        $user = $this->user;
        Mail::send('email.UserPasswordChange', function ($message) use ($user) {
            $message->to(
                $user->email,
                $user->last_name." ".$user->first_name
            )->subject('Password was changed');
        });

        return true;
    }
}
