<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Authorization extends Model
{
    public $table = 'authorization';
    public $timestamps = false;

    protected $hidden = ['password'];

    public function user ()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function passwordReset ()
    {
        return $this->hasMany(PasswordReset::class, 'auth_id', 'id');
    }
}