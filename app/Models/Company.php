<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    use SoftDeletes;

    public $table = 'companies';
    public $appends = ['workPositionsIds'];

    public function getWorkPositionsIdsAttribute()
    {
        $workPositions = DB::table('company_work_positions')->where('company_id', $this->id)->get();
        $output = [];

        foreach ($workPositions as $workPosition) {
            array_push($output, $workPosition->work_position_id);
        }

        return $output;
    }

    
}
