<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;

    public $table = 'notifications';
    public $timestamps = true;
    public $hidden = ['user_id', 'notification_type'];
    public $appends = ['type'];

    public function getTypeAttribute ()
    {
        switch ($this->notification_type) {
            case 1: return 'info';
            case 2: return 'warning';
            case 3: return 'danger';
            case 0: return 'other';
            default: return null;
        }
    }

    public static function newUser (User $user)
    {
        $notification = new Notification();

        $notification->notification_type = 1;
        $notification->title = 'New user created: ' . $user->first_name.' '.$user->last_name;
        $notification->content = "New user was created.";

        return $notification;
    }

    public static function userEdit (User $user)
    {
        $notification = new Notification();

        $notification->notification_type = 1;
        $notification->title = 'User ('.$user->id.') was edited';
        $notification->content = "User was modified";

        return $notification;
    }

    public static function userDeleted (User $user)
    {
        $notification = new Notification();

        $notification->notification_type = 1;
        $notification->title = 'User ('.$user->id.') was deleted';
        $notification->content = $user->first_name.' '.$user->last_name.' was deleted';

        return $notification;
    }

    /**
     * @param {User} $user
     * @param {boolean} $version
     * true - admin version, false - user version
     */
    public static function userPasswordChange (User $user, $version = false)
    {
        $notification = new Notification();

        $notification->notification_type = 1;

        if ($version) {
            $notification->title = 'User ('.$user->id.') had changed password';
            $notification->content = $user->first_name.' '.$user->last_name.' changed password';
        } else {
            $notification->title = 'Your password was changes';
            $notification->content = 'Lately your password was changed';
        }

        return $notification;
    }
}