<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    public $table = 'password_reset';
    public $timestamps = false;

    public function auth ()
    {
        return $this->hasOne(Authorization::class, 'id', 'auth_id');
    }
}