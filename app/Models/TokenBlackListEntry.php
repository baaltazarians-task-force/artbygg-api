<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokenBlackListEntry extends Model
{
    public $table = 'token_blacklist';
    public $timestamps = false;
}
