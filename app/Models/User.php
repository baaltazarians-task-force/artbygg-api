<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

    public $table = 'users';
    public $appends = ['groupsIds'];
    public $with = ['status'];

    protected $hidden = ['pivot'];

    const partialSelectShow = ['first_name', 'last_name', 'email', 'id'];
    const partialSelectHide = ['status', 'groupsIds'];

    public function auth ()
    {
        return $this->hasOne(Authorization::class, 'user_id', 'id');
    }

    public function groups ()
    {
        return $this->belongsToMany(UserGroup::class, 'user_groups_pivot');
    }

    public function company ()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function notifications ()
    {
        return $this->hasMany(Notification::class, 'user_id', 'id');
    }

    public function status ()
    {
        return $this->hasOne(UserStatus::class, 'user_id', 'id');
    }

    public function workPosition ()
    {
        return $this->hasOne(WorkPosition::class, 'id', 'work_position_id');
    }

    public function getGroupsIdsAttribute ()
    {
        $groups = $this->groups()->get();
        $groupsIds = [];

        foreach ($groups as $group) {
            array_push($groupsIds, $group->id);
        }

        return $groupsIds;
    }

    public function getPermissionsAttribute ()
    {
       $groups = $this->groups()->get();

       $loadedPermissions = [];
       $permissions = [];

        foreach ($groups as $group) {
            $groupPermissions = $group->getPermissionsWithStates();
            if (sizeof($groupPermissions)) {
                foreach ($groupPermissions as $groupPermission) {
                    if (isset($loadedPermissions[$groupPermission->id])) continue;

                    array_push($permissions, $groupPermission->attributesToArray());
                    $loadedPermissions[$groupPermission->id] = true;
                }
            }
        }

        return $permissions;
    }
}
