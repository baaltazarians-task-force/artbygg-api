<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class UserGroup extends Model
{
    use SoftDeletes;

    public $table = 'user_groups';
    public $timestamps = false;
    protected $hidden = ['pivot'];

    const partialSelectShow = ['id', 'slug', 'description'];
    const partialSelectHide = [];

    public function permissions ()
    {
        return $this->belongsToMany(Permission::class, 'user_groups_permissions_pivot');
    }

    public function getPermissionsAttribute () 
    {
        return $this->getPermissionsWithStates();
    }

    public function getPermissionsWithStates ()
    {
        $permissions = $this->permissions()->get();
        $states = DB::table('user_groups_permissions_pivot')->where('user_group_id', $this->id)->get();

        for ($i = 0; $i < sizeof($permissions); $i++) {
            $state = $states->where('permission_id', $permissions[$i]->id)->first();
            $permissions[$i]['create'] = $state->create ?? null;
            $permissions[$i]['read']   = $state->read ?? null;
            $permissions[$i]['update'] = $state->update ?? null;
            $permissions[$i]['delete'] = $state->delete ?? null;
            $permissions[$i]['other']  = $state->other ?? null;
        }

        return $permissions;
    }
}
