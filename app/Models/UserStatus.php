<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    public $table = 'user_status';
    public $timestamps = true;
    protected $hidden = ['created_at', 'updated_at', 'id'];
}
