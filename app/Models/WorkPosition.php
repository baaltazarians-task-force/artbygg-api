<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkPosition extends Model
{
    public $table = 'work_positions';
    public $timestamps = false;   
}
