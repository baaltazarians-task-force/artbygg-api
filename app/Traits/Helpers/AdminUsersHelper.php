<?php

namespace App\Traits\Helpers;

use App\Models\User;

trait AdminUsersHelper
{
    function getAdminsEmails ()
    {
        return 
            User::whereHas('groups', function ($query) {
                $query->where('slug', 'administrator');
            })
                ->get()
                ->map(function ($row) { return $row->email; });
    }

    function getAdminsIds ()
    {
        return 
            User::whereHas('groups', function ($query) {
                $query->where('slug', 'administrator');
            })
                ->get()
                ->map(function ($row) { return $row->id; });
    }
}