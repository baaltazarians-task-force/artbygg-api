<?php

namespace App\Traits\Helpers;

use App\Models\User;

trait PasswordHelper
{
    /**
     * @param {int} $length
     */
    function generatePassword (int $length = 10)
    {
        $password = "";
        while (strlen($password) < $length) {
            $password .= chr(mt_rand(35, 125));
        }
        return $password;
    }
}