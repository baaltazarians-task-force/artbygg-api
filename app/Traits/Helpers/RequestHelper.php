<?php

namespace App\Traits\Helpers;

use App\Models\User;
use Illuminate\Http\Request;


trait RequestHelper
{
    /**
     * @param {Request} $req
     */
    function retriveUID (Request $req, bool $create = false)
    {
        $uid = $req['jwt-token-payload']['uid'];
        return $create ? User::find($uid) : $uid;
    }
}
