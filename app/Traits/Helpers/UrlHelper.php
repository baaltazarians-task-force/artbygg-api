<?php

namespace App\Traits\Helpers;

use App\Models\PasswordReset;

trait UrlHelper
{
    function createSPAUrl ($url)
    {
        return env('APP_VUE_URL').$url;
    }

    function createResetPasswordUrl (PasswordReset $passReset)
    {
        return $this->createSPAUrl('pages/reset-password/'.base64_encode($passReset->reset_token));
    }
}