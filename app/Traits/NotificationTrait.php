<?php

namespace App\Traits;

use App\Models\Notification;
use App\Models\User;
use App\Traits\Helpers\AdminUsersHelper;
use Illuminate\Support\Facades\DB;

trait NotificationTrait
{
    use AdminUsersHelper;

    // when new user is created to admins
    /**
     * @param {User} $user
     */
    function newUserNotification (User $user)
    {
        DB::beginTransaction();
        foreach ($this->getAdminsIds() as $id) {
            $notify = Notification::newUser($user);
            $notify->user_id = $id;

            if ($notify->save()) {
                DB::rollBack();
                return false;
            }
        }
        
        DB::commit();
        return true;
    }

    // when user is modified to admins
    /**
     * @param {User} $user
     */
    function userModifyNotification (User $user)
    {
        DB::beginTransaction();
        foreach ($this->getAdminsIds() as $id) {
            $notify = Notification::userEdit($user);
            $notify->user_id = $id;

            if ($notify->save()) {
                DB::rollBack();
                return false;
            }
        }

        DB::commit();
        return true;
    }

    // when user is deleted to admins
    /**
     * @param {User} $user
     */
    function userDeletionNotification (User $user)
    {
        DB::beginTransaction();
        foreach ($this->getAdminsIds() as $id) {
            $notify = Notification::userDeleted($user);
            $notify->user_id = $id;

            if ($notify->save()) {
                DB::rollBack();
                return false;
            }
        }

        DB::commit();
        return true;
    }

    // when user changes password
    /**
     * @param {User} $user
     * @param {bool} $toAdmins
     */
    function userChangesPasswordNotification (User $user, bool $toAdmins = false)
    {
        $recipients = $toAdmins ? $this->getAdminsIds() : [$user->id];

        DB::beginTransaction();
        foreach ($recipients as $id) {
            $notify = Notification::userPasswordChange($user, $toAdmins);
            $notify->user_id = $id;

            if ($notify->save()) {
                DB::rollBack();
                return false;
            }
        }

        DB::commit();
        return true;
    }

}