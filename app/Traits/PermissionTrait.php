<?php

namespace App\Traits;

trait PermissionTrait
{
    
    /**
     * @param {Array} $userPermissions
     * @param {Array} $requiredPermissions
     * @param {Array} $partialPermissions
     */
    function checkPermissions (array $userPermissions = [], array $requiredPermissions = [], array $partialPermissions = null)
    {        
        $output = [
            'passes'  => false, // used for full access
            'partial' => false, // used for restricted access
        ];

        $passedTests = $this->testPermissions($userPermissions, $requiredPermissions);

        if ($passedTests) {
            $output['passes'] = true;
        }

        if (!$passedTests && is_array($partialPermissions)) {
            $output['partial'] = $this->testPermissions($userPermissions, $partialPermissions);
        }

        return $output;
    }

    /**
     * @param {Array} $permissions
     * @param {Array} $required
     */
    function testPermissions (array $permissions, array $required)
    {
        $tests = 0;
        $toPass = 0;
        $optPass = false;

        foreach ($required as $row) {
            $raw   = explode('.', $row);
            $optMode  = substr($raw[0], 0, 1) === '!';
            $slug  = $optMode ? substr($raw[0], 1) : $raw[0];
            $field = $raw[1];

            if ($optMode) {
                $optPass = true;
            } else {
                $toPass++;
            }

            foreach ($permissions as $perm) {
                if ($perm['slug'] === $slug) {
                    if ($perm[$field])
                        $tests++;

                    break;
                }
            }
        }

        if ($optPass) {
            if ($toPass == 0) $toPass = 1;
            return $tests >= $toPass;
        }

        return $tests >= $toPass;
    }
}