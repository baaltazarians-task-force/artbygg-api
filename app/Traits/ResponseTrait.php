<?php

namespace App\Traits;

trait ResponseTrait
{

    /**
     * @param {mixed} $message
     * @param {int} $errorCode
     */
    function responseSuccess($message = 'ok', int $errorCode = 200)
    {
        return response()->json(['status' => true, 'result' => $message], $errorCode);
    }

    /**
     * @param {mixed} $message
     * @param {int} $errorCode
     */
    function responseError($message, int $errorCode = 400)
    {
        return response()->json(['status' => false, 'result' => $message], $errorCode);
    }

    /**
     * @param {mixed} $message
     * @param {int} $errorCode
     */
    function responseErrorServer($message, int $errorCode = 500) 
    {
        return response()->json(['status' => false, 'result' => $message], $errorCode);
    }

    /**
     * @param {mixed} $message
     * @param {int} $errorCode
     */
    function responseUnauthorized($message = 'Unauthorized', int $errorCode = 401)
    {
        return response()->json(['status' => false, 'result' => $message], $errorCode);
    }

}