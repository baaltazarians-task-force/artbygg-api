<?php

namespace App\Traits;

use App\Mail\NewUser;
use App\Mail\NotifyNewUser;
use App\Mail\PasswordReset;
use App\Mail\UserPasswordChange;
use App\Models\User;
use App\Traits\Helpers\AdminUsersHelper;

trait SendEmailTrait
{
    use AdminUsersHelper;

    /**
     * @param {User} $newUser
     */
    function sendNewUserEmail (User $newUser)
    {
        $message = new NewUser($newUser, $this->getAdminsEmails());
        $message->send();

        return true;
    }

    /**
     * @param {User} $newUser
     * @param {string} $temporaryPassword
     */
    function sendNewUserAccountNotificationEmail (User $newUser, string $temporaryPassword)
    {
        $message = new NotifyNewUser($newUser, $temporaryPassword);
        $message->send();

        return true;
    }

    /**
     * @param {User} $newUser
     */
    function sendUserPasswordChangeEmail (User $user)
    {
        $message = new UserPasswordChange($user);
        $message->send();

        return true;
    }

    function sendUserPasswordResetLinkEmail (User $user, string $url)
    {
        $message = new PasswordReset($url, $user);
        $message->send();

        return $true;
    }

}