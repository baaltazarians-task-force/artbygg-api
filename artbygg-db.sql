-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `authorization`;
CREATE TABLE `authorization` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `password` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_expires` tinyint(1) NOT NULL,
  `password_reset_on_logon` tinyint(1) NOT NULL,
  `password_expiration_date` date DEFAULT NULL,
  `locked` tinyint NOT NULL,
  PRIMARY KEY (`id`),
  KEY `authorization_user_id_foreign` (`user_id`),
  CONSTRAINT `authorization_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `authorization` (`id`, `user_id`, `password`, `password_expires`, `password_reset_on_logon`, `password_expiration_date`, `locked`) VALUES
(1,	1,	'$2y$10$ZyC8M3wJEW0UCjcjJOQ9.OwWWLDbr.Yvy6ICuuEzkKKl0YaVKY5je',	0,	0,	NULL,	0),
(12,	13,	'$2y$10$.hELg7pKgvJdhqlcnlnXGuulGIZAAomDci/bB5mBDJpPk43gT/y7.',	0,	1,	NULL,	0);

DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_number` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_iso_code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `companies` (`id`, `name`, `tax_number`, `city`, `postal_code`, `street`, `country_iso_code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'Serpent Sp. z o.o.',	'106-00-00-062',	'Kraków',	'11-111',	'Krakowska 22',	'PL',	'2020-10-17 16:04:41',	'2020-10-17 16:04:41',	NULL);

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2020_10_14_000000_create_companies_table',	1),
(2,	'2020_10_14_000001_create_work_positions_table',	1),
(3,	'2020_10_14_000002_create_user_groups_table',	1),
(4,	'2020_10_14_000003_create_users_table',	1),
(5,	'2020_10_14_000004_create_user_settings_table',	1),
(6,	'2020_10_14_000005_create_permissions_table',	1),
(7,	'2020_10_14_000006_create_authorization_table',	1),
(8,	'2020_10_14_000007_create_user_groups_permissions_pivot_table',	1),
(9,	'2020_10_14_00008_create_user_groups_pivot_table',	1),
(10,	'2020_10_14_00009_create_token_blacklist_table',	1);

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `permissions` (`id`, `slug`, `description`) VALUES
(1,	'admin',	'Administrative permission'),
(2,	'users',	'Permission related to user management'),
(3,	'orders',	NULL),
(4,	'jobs',	NULL),
(5,	'projects',	NULL),
(6,	'reports',	NULL),
(7,	'groups',	NULL);

DROP TABLE IF EXISTS `token_blacklist`;
CREATE TABLE `token_blacklist` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `token_blacklist` (`id`, `token`) VALUES
(1,	'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE2MDI3MTU1MjAsImlzcyI6ImxvY2FsaG9zdCIsImlhdCI6MTYwMjcxMTkyMH0.sKVl-2WrK7jOP5R-MyJNoE-0kvG53kLMSCDdFCYYhig');

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `user_groups` (`id`, `slug`, `description`, `deleted_at`) VALUES
(1,	'administrator',	NULL,	NULL),
(2,	'users',	NULL,	NULL),
(4,	'test_group',	NULL,	NULL),
(8,	'clients v2',	NULL,	'2020-10-17 18:16:39');

DROP TABLE IF EXISTS `user_groups_permissions_pivot`;
CREATE TABLE `user_groups_permissions_pivot` (
  `user_group_id` bigint unsigned NOT NULL,
  `permission_id` bigint unsigned NOT NULL,
  `create` tinyint(1) NOT NULL,
  `read` tinyint(1) NOT NULL,
  `update` tinyint(1) NOT NULL,
  `delete` tinyint(1) NOT NULL,
  `other` tinyint(1) NOT NULL,
  KEY `user_groups_permissions_pivot_user_group_id_foreign` (`user_group_id`),
  KEY `user_groups_permissions_pivot_permission_id_foreign` (`permission_id`),
  CONSTRAINT `user_groups_permissions_pivot_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  CONSTRAINT `user_groups_permissions_pivot_user_group_id_foreign` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `user_groups_permissions_pivot` (`user_group_id`, `permission_id`, `create`, `read`, `update`, `delete`, `other`) VALUES
(1,	1,	0,	0,	0,	0,	1),
(1,	2,	1,	1,	1,	1,	1),
(1,	3,	1,	1,	1,	1,	1),
(1,	4,	1,	1,	1,	1,	1),
(1,	5,	1,	1,	1,	1,	1),
(1,	6,	1,	1,	1,	1,	1),
(1,	7,	1,	1,	1,	1,	1),
(8,	3,	1,	1,	1,	1,	1),
(8,	1,	0,	0,	0,	0,	1);

DROP TABLE IF EXISTS `user_groups_pivot`;
CREATE TABLE `user_groups_pivot` (
  `user_id` bigint unsigned NOT NULL,
  `user_group_id` bigint unsigned NOT NULL,
  KEY `user_groups_pivot_user_id_foreign` (`user_id`),
  KEY `user_groups_pivot_user_group_id_foreign` (`user_group_id`),
  CONSTRAINT `user_groups_pivot_user_group_id_foreign` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`id`),
  CONSTRAINT `user_groups_pivot_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `user_groups_pivot` (`user_id`, `user_group_id`) VALUES
(1,	1),
(13,	2);

DROP TABLE IF EXISTS `user_settings`;
CREATE TABLE `user_settings` (
  `user_id` bigint unsigned NOT NULL,
  `settings` json DEFAULT NULL,
  KEY `user_settings_user_id_foreign` (`user_id`),
  CONSTRAINT `user_settings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` bigint unsigned DEFAULT NULL,
  `work_position_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_company_id_foreign` (`company_id`),
  KEY `users_work_position_id_foreign` (`work_position_id`),
  CONSTRAINT `users_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `users_work_position_id_foreign` FOREIGN KEY (`work_position_id`) REFERENCES `work_positions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `company_id`, `work_position_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'Dawid',	'Cziernok',	'dawid.cziernok@protonmail.com',	NULL,	NULL,	NULL,	'2020-10-14 21:44:21',	'2020-10-14 21:44:21',	NULL),
(13,	'Janusz',	'Kowalski',	'janusz.kowalski@testowo.com',	NULL,	1,	NULL,	'2020-10-17 15:21:59',	'2020-10-17 16:07:51',	NULL);

DROP TABLE IF EXISTS `work_positions`;
CREATE TABLE `work_positions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `company_id` bigint unsigned DEFAULT NULL,
  `pivot` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `work_positions_company_id_foreign` (`company_id`),
  CONSTRAINT `work_positions_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2020-10-17 18:41:46
