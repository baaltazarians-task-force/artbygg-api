<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class addPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                "slug" => "admin",
                "description" => "Administrative permissions"
            ],
            [
                "slug" => "users",
                "description" => "Permission related to user management"
            ],
            [
                "slug" => "orders",
                "description" => "Permission related to order management"
            ],
            [
                "slug" => "jobs",
                "description" => "Permission related to job management"
            ],
            [
                "slug" => "projects",
                "description" => "Permission related to project management"
            ],
            [
                "slug" => "reports",
                "description" => "Permission related to creating reports"
            ],
            [
                "slug" => "groups",
                "description" => "Permission related to user groups management"
            ],
            [
                "slug" => 'companies',
                "description" => "Permission related to companies management"
            ]
        ]);
    }
}
