<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class addUserGroups extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_groups')->insert([
            [
                "slug" => "administrator",
                "description" => "Administrator group",
                "deleted_at" => null
            ],
            [
                "slug" => "user",
                "description" => "Generic user group",
                "deleted_at" => null
            ]
        ]);
    }
}
