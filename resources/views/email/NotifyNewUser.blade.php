<h1>Hello {{ $user->first_name }} {{ $user->last_name }}</h1>

<p>Your login: <strong>{{ $user->email }}</strong></p>
<p>
  Temporary password: <strong>{{ $password }}</strong><br>
  <small>Must be changed on first logon</small>
</p>

<p>You can login here: <a href="http://apk.w-budowie.eu/pages/login">http://apk.w-budowie.eu/pages/login</a></p>
