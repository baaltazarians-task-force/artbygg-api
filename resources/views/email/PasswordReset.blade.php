<h3>Password reset</h3>
<p>Click this link to setup a new password for your account</p>
<a href="#/">{{ $url }}</a>

<p>Link is valid for 30 minutes</p>
