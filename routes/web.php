<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'v1'], function () use ($router) {

    // for testing
    $router->post('/create-password-hash', 'TestController@createHash');
    $router->get('/test-attach', 'TestController@testAttach');

    // app paths, ie.: logon page bg image, password reset url etc.
    $router->group(['prefix' => 'app'], function () use ($router) {
        $router->get('/logon-background-image', 'AppController@bgImg');
    });


    // authorization service
    $router->group(['prefix' => 'authorization'], function () use ($router) {
        $router->post('/login',                 'AuthorizationController@login');                   // done
        $router->post('/password-reset',        'AuthorizationController@passwordReset');           // done
        $router->get('/password-reset/{token}', 'AuthorizationController@checkPasswordReset');      // done
        $router->post('/password-reset/{token}','AuthorizationController@passwordResetHandler');

        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->get('/logout',             'AuthorizationController@logout');              // done
            $router->get('/user-info',          'AuthorizationController@userInfo');            // done
            $router->get('/user-permissions',   'AuthorizationController@userPermissions');     // done
            $router->get('/check-token',        'AuthorizationController@checkToken');          // done
            $router->get('/extend-token',       'AuthorizationController@extendToken');         // done?
            $router->post('/change-password',   'AuthorizationController@changePassword');      // done
        });
    });

    // notification service
    $router->group(['prefix' => 'notification', 'middleware' => 'auth'], function () use ($router) {
        $router->get('/list',       'NotificationController@getNotifications');
    });

    // user service
    $router->group(['prefix' => 'user', 'middleware' => ['auth', 'permissions', 'validate']], function () use ($router) {
        $router->get('/list',       'UserController@getUsers');                                 // done
        $router->get('/{id}',       'UserController@getUser');                                  // done
        $router->put('/',           'UserController@createUser');                               // done
        $router->patch('/{id}',     'UserController@updateUser');                               // done
        $router->delete('/{id}',    'UserController@deleteUser');                               // done
    });

    // groups service
    $router->group(['prefix' => 'group', 'middleware' => ['auth', 'permissions', 'validate']], function () use ($router) {
        $router->get('/list',       'GroupController@getGroups');                               // done
        $router->get('/{id}',       'GroupController@getGroup');                                // done
        $router->put('/',           'GroupController@createGroup');                             // done
        $router->patch('/{id}',     'GroupController@updateGroup');                             // done
        $router->delete('/{id}',    'GroupController@deleteGroup');                             // done
    });

    // permissions service
    $router->group(['prefix' => 'permission', 'middleware' => ['auth', 'permissions']], function () use ($router) {
        $router->get('/list',   'PermissionController@getPermissions');
    });

    // companies service
    $router->group(['prefix' => 'company', 'middleware' => ['auth', 'permissions']], function () use ($router) {
        $router->get('/list',       'CompanyController@getCompanies');
        $router->get('/{id}',       'CompanyController@getComapny');
        $router->put('/',           'CompanyController@createComapny');
        $router->patch('/{id}',     'CompanyController@updateComapny');
        $router->delete('/{id}',    'CompanyController@deleteComapny');
    });

    // workplaces service
    $router->group(['prefix' => 'work-position', 'middleware' => ['auth', 'permissions']], function () use ($router) {
        $router->get('/list', 'WorkPositionController@getWorkPositions');
    });

});
